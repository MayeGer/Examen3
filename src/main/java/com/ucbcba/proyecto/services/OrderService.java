package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Order;

public interface OrderService {

    Iterable<Order> findAllOrder();

    Order getOrderById(Integer id);

    Order saveOrder(Order order);

    void deleteOrder(Integer id);
}
